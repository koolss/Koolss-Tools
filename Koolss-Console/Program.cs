﻿
using Koolss_Core.Util;
using Koolss_Log;
using System;
using System.IO;

namespace Koolss_Console
{
    class Program
    {
        //private static ILog logger =LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private static ILoggerRepository LoggerRepository;
        static void Main(string[] args)
        {
            //ILoggerRepository LoggerRepository = LogManager.CreateRepository("Log4netConsolePractice");
            //XmlConfigurator.ConfigureAndWatch(LoggerRepository, new FileInfo("log4net.config"));
            //var log = LogManager.GetLogger(LoggerRepository.Name, typeof(Program));
            //log4net日志
            //log4net.ILog logInfo = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            //log.Info("info");
            //log.ErrorFormat("Error");
            //log.Debug("Debug");
            LogUtil.Info("InfoFormat");
            LogUtil.Error("ErrorFormat");
            LogUtil.DebugFormat("DebugFormat：{0}",IdUtil.randomGuid());
            Console.WriteLine(Guid.NewGuid());
            Console.ReadLine();
        }
    }
}
