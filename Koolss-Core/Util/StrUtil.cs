﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_Core.Util
*│　类    名： StrUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：字符串工具类
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/15 星期五 14:29:02
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss_Core.Util
{
    /// <summary>
    /// 字符串工具类
    /// </summary>
    public class StrUtil
    {
        /// <summary>
        /// 字符串是否为空白 空白的定义如下：
        /// 1、为null
        /// 2、为不可见字符（如空格）
        /// 3、""
        /// </summary>
        /// <param name="str">被检测的字符串</param>
        /// <returns>是否为空</returns>
        public static bool isBlank(string str)
        {
            int length;
            if ((str == null) || ((length = str.Length) == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 创建StringBuilder对象
        /// </summary>
        /// <returns>StringBuilder对象</returns>
        public static StringBuilder builder()
        {
            return new StringBuilder();
        }
    }
}
