﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_Core.Util
*│　类    名： DictUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：Dictionary工具类
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/15 星期五 15:20:52
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss_Core.Util
{
    /// <summary>
    /// Dictionary工具类
    /// </summary>
    public class DictUtil
    {
        /// <summary>
        /// 判断字典是否为空
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dict">字典</param>
        /// <returns>是否为空</returns>
        public static bool isEmpty<TKey, TValue>(IDictionary<TKey,TValue> dict)
        {
            return dict == null || dict.Count == 0;
        }

        /// <summary>
        /// 判断字典是否不为空
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dict">字典</param>
        /// <returns>是否不为空</returns>
        public static bool isNotEmpty<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            return dict != null || dict.Count != 0;
        }

        /// <summary>
        /// 字典排序（默认升序）
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dict">字典</param>
        /// <returns>有序字典</returns>
        public static IDictionary<TKey, TValue> sort<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            if (dict==null)
            {
                throw new ArgumentNullException("排序字典不能为NULL");
            }
            return new SortedDictionary<TKey, TValue>(dict);
        }

        /// <summary>
        /// 字典排序(自定义排序)
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dictionary">无序字典</param>
        /// <param name="comparer">自定义排序方法</param>
        /// <returns>有序字典</returns>
        public static IDictionary<TKey, TValue> Sort<TKey, TValue>(IDictionary<TKey, TValue> dictionary, IComparer<TKey> comparer)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("排序字典不能为NULL");
            }

            if (comparer == null)
            {
                throw new ArgumentNullException("自定义排序方法为空,不能排序");
            }

            return new SortedDictionary<TKey, TValue>(dictionary, comparer);
        }

        /// <summary>
        /// 字典排序(依据值默认排序)
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dictionary">无序字典</param>
        /// <returns>有序字典</returns>
        public static IDictionary<TKey, TValue> SortByValue<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
        {
            return dictionary.OrderBy(v => v.Value).ToDictionary(item => item.Key, item => item.Value);
        }

        /// <summary>
        /// 字典排序(依据值自定义排序)
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dictionary">无序字典</param>
        /// <param name="comparer">自定义排序方法</param>
        /// <returns>有序字典</returns>
        public static IDictionary<TKey, TValue> SortByValue<TKey, TValue>(IDictionary<TKey, TValue> dictionary, IComparer<TValue> comparer)
        {
            return dictionary.OrderBy(v => v.Value, comparer).ToDictionary(item => item.Key, item => item.Value);
        }

        /// <summary>
        /// 将字典转为字符串
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dictionary">字典</param>
        /// <param name="separator">entry之间的连接符</param>
        /// <param name="keyValueSeparator">kv之间的连接符</param>
        /// <returns>连接字符串</returns>
        public static string join<TKey, TValue>(IDictionary<TKey, TValue> dictionary, string separator, string keyValueSeparator)
        {
            return join(dictionary, separator, keyValueSeparator, false);
        }

        /// <summary>
        /// 将字典转为字符串
        /// </summary>
        /// <typeparam name="TKey">Key类型</typeparam>
        /// <typeparam name="TValue">Value类型</typeparam>
        /// <param name="dictionary">字典</param>
        /// <param name="separator">entry之间的连接符</param>
        /// <param name="keyValueSeparator">kv之间的连接符</param>
        /// <param name="isIgnoreNull">是否忽略null的键和值</param>
        /// <returns></returns>
        public static string join<TKey, TValue>(IDictionary<TKey, TValue> dictionary, string separator, string keyValueSeparator, bool isIgnoreNull)
        {
            StringBuilder stringBuilder = StrUtil.builder();
            bool isFirst = true;
            foreach (var key in dictionary.Keys.Distinct())
            {
                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    stringBuilder.Append(separator);
                }
                stringBuilder.Append(key).Append(keyValueSeparator).Append(dictionary[key]);
            }
            return stringBuilder.ToString();
        }
    }
}
