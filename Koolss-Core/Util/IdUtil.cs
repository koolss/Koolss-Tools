﻿using System;
using System.Collections.Generic;
using System.Text;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_Core.Util
*│　类    名： IdUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：ID生成器工具类，此工具类中主要封装：
*            1.唯一性ID生成器：UUID
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/14 21:40:50
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss_Core.Util
{
    /// <summary>
    /// ID生成器工具类
    /// </summary>
    public class IdUtil
    {
        /// <summary>
        /// 获取随机Guid
        /// </summary>
        /// <returns>随机Guid</returns>
        public static string randomGuid() {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 简化的Guid，去掉了下划线
        /// </summary>
        /// <returns>简化的UUID，去掉了横线</returns>
        public static string simpleGuid() {
            return Guid.NewGuid().ToString("N");
        }
    }
}
